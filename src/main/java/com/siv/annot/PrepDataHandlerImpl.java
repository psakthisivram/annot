package com.siv.annot;

public class PrepDataHandlerImpl implements PrepDataHandler {

	public PrepDataHandlerImpl(){
		System.out.println("Test");
	}
	
	@Override
	public void prepareData(String name) {
		System.out.println("Preparing data "+name+". Success !");
	}

}
