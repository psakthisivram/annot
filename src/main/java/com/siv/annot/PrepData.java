package com.siv.annot;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Target({METHOD, FIELD, TYPE})
@Handler(handler = PrepDataHandlerImpl.class)
@Retention(RUNTIME)
public @interface PrepData {
	String name() default "";
}