package com.siv.annot;

public interface PrepDataHandler {
	public void prepareData(String name);
}
