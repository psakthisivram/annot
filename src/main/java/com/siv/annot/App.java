package com.siv.annot;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Hello world!
 *
 */
public class App 
{
	public App() throws InstantiationException, IllegalAccessException{
		Method[] methods = this.getClass().getMethods();
		
		for(Method m : methods){
			Annotation[] annotations = m.getAnnotations();
			
			for(Annotation a : annotations){
				System.out.println(a.annotationType().getName());
				if(a.annotationType().getName().equals("com.siv.annot.PrepData")){
					    Class<? extends Annotation> pd = a.annotationType();

						Handler[] handlers = pd.getClass().getAnnotationsByType(Handler.class);
						System.out.println(Arrays.toString(pd.getClass().getDeclaredFields()));
						for(Handler h : handlers){
							
							PrepDataHandlerImpl pdh = ((PrepDataHandlerImpl) h.handler().newInstance());
							pdh.prepareData("");
						}
				}
			}
		}
	}
	
	@PrepData(name = "test")
    public void test(){
    	
    }
	
	public static void main( String[] args ) throws InstantiationException, IllegalAccessException
    {
        App a = new App();
    }
}
